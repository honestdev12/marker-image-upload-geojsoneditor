import * as Sequelize from 'sequelize';

import { DataTypeAbstract, DefineAttributeColumnOptions } from "sequelize";

type SequelizeAttribute = string | DataTypeAbstract | DefineAttributeColumnOptions;

export type SequelizeAttributes<T extends { [key: string]: any }> = {
    [P in keyof T]: SequelizeAttribute
};

export interface UserAttributes {
    id?: number;
    name: string;
    password: string;
    isAdmin?: boolean;
    isEdit?: boolean;
    isView?: boolean;
    createdAt?: Date;
    updatedAt?: Date;
}

export interface UserInstance extends Sequelize.Instance<UserAttributes>, UserAttributes {
}

export const UserFactory = (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes): Sequelize.Model<UserInstance, UserAttributes> => {
    const attributes: SequelizeAttributes<UserAttributes> = {
        name: {
            type: DataTypes.STRING,
            unique: true
        },
        password: {
            type: DataTypes.STRING
        },
        isAdmin: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        isEdit: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        isView: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        }
    };

    const User = sequelize.define<UserInstance, UserAttributes>('User', attributes);

    return User;
};
